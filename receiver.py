from microbit import *  # Basic for interfacing with micro:bit
import radio            # For using radio

radio.config(group=7)   # config and assign group
radio.on()              # Switch it on

while True:             # Do forever
    message = radio.receive()   # Get the message from the radio
    if message:                 # If the line is not empty
        print(message)          # Just print it to serial. It's CSV and accessable @ 115200 baud
        sleep(70)               # Sleep a bit. Reduces traffic
