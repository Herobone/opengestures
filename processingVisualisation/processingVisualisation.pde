import processing.serial.*;

Serial port;
String input;

int x;
int y;
int z;
int g;

boolean leftClick;
boolean rightClick;

void setup(){
  init();
  size(1000, 1000);
  background(180);
  translate(width/2, height/2);
  rectMode(CENTER);


}

void draw(){
  
  read();
  
  parseInput();
  
  render();

}

void init() {
  printArray(Serial.list());
  String portName = Serial.list()[0];
  port = new Serial(this, portName, 115200);
}

void read() {
  if (port.available() > 0) {
    input = port.readStringUntil(10);
    print();
  }
}

void print() {
  println(input);
  fill(50);
  stroke(255);
  rotateX(PI/8);
}

void parseInput() {

  //parse string into x, y, z, g
  if (input != null) {

    String[] inputArray = input.split(",");

    x = Integer.parseInt(inputArray[0]); 
    y = Integer.parseInt(inputArray[1]); 
    z = Integer.parseInt(inputArray[2]); 
    g = Integer.parseInt(inputArray[3]);
    
    leftClick = (Integer.parseInt(inputArray[4]) == 1);
    rightClick =  (Integer.parseInt(inputArray[5]) == 1);

    
  } catch (Exception NumberParseException) {
    
    println("Failed to parse input to x y z g"); //if somethings wrong with parsing
    
    

      println("Failed to parse input to x y z g");



      x = 0;
      y = 0;
      z = 0;
      g = 0;
    }
  } else {

    x = 0;
    y = 0;
    z = 0;
    g = 0;
  }
}


void render() {

  fill(256);

  stroke(128);

  line(0, 0, x, y);

}
