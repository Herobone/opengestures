import serial
import serial.tools.list_ports as list_ports
import pyautogui

PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 0.1

# searches the serial port for the micro:bit
def find_comport(pid, vid, baud):
    ''' return a serial port '''
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None

def makeTheMouse(inputtt):
    global ser_micro
    if not (inputtt[0] < -200 and inputtt[0] > 200 and inputtt[1] < 200 and inputtt[1] > -200): # if Hand is not in the deadzone proceed
        pyautogui.moveRel(inputtt[0] / 90, inputtt[1] / 90) # move the mouse relative dependent on how it's tilted

        if inputtt[3] is 7:
            pyautogui.click()                       # if shake is detected interpet it as a click
        ser_micro.reset_input_buffer()              # delete serial buffer, because it takes some time to process mouse. Otherwise there's a big lag

def lineSplitter(line = str()):
    splitted = line.replace('\r\n', '').split(',')  # remove newline characters and split the csv
    converted = []
    for thing in splitted:                  # Cycle through all parts
        try:
            converted.append(int(thing))    # Add the int that's parsed from the sting to a list
        except:
            pass
    return converted

def main():
    global ser_micro
    print('looking for microbit')
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)    # Find a micro:bit device
    if not ser_micro:
        print('microbit not found')
        return
    print('opening and monitoring microbit port')
    ser_micro.open()                        # open serial connection to micro:bit
    try:
        while True:
            line = ser_micro.readline().decode('utf-8')
            if line:                        # If it isn't a blank line
                out = lineSplitter(line)    # Split and convert the input to ints
                print(out)                  # Just debug
                makeTheMouse(out)           # Call the mouse handler
    except KeyboardInterrupt:               # When script is cancelled
        ser_micro.close()
        print("Exiting")

if __name__ == "__main__":
    main()
