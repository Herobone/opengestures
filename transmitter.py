from microbit import *  # Basic lib for interfacing with the micro:bit
import radio            # For sending values
import neopixel         # For controlling the NeoPixel ring around

radio.config(group=7)   # Set the send group for the micro:bit
radio.on()              # Switch it on

ring = neopixel.NeoPixel(pin0, 24) # Configure the NeoPixel ring. It's connected on pin0 and has 24 LEDs

# variables to store the last colors and states
lastr = 0
lastg = 0
lastb = 0
laststate = "up"
disabled = False

'''
Controls the LED Ring
Lights up the LEDs between "start" and "stop" with the specified color
@param start    int     Start index written on the ring
@param stop     int     End index written on the ring
'''
def lightUp(start, stop, r, g, b):
    global lastr, lastg, lastb, laststate
    # If current color is the same as before and the state is the same don't continue. Reduces flickering
    if (lastb == b and lastg == g and lastr == r) and (laststate is accelerometer.current_gesture()):
            return
    
    ring.clear()                        # Clear the ring so you don't hve mixing colors
    if start > stop:                    # If the start index is larger than the stop index
        for i in range(start - 1,24):   # Control the first part of the leds ...
            ring[i] = (r, g, b)
        for i in range(0,stop):         # ... and then the second. Otherwise you could not activate e.g. 24 and 1 at the same time
            ring[i] = (r, g, b)
    else:                               # If it's not like that
        for i in range(start - 1,stop): # Cycle normal through the leds
            ring[i] = (r, g, b)         # and set their rgb values accordingly
    ring.show()                         # display the colors to the ring. When your not calling this nothing will show
    lastr = r                           # set the last values so we can compare later
    lastg = g
    lastb = b
    laststate = accelerometer.current_gesture()

while True:
    sendMe = str()                          # Initialize a new string
    sendMe = sendMe + str(accelerometer.get_x()) + "," + str(accelerometer.get_y()) + "," + str(accelerometer.get_z())
    if(disabled):                           # If the Controller is disabled make the ring red
        lightUp(1, 24, 255, 0,0)            # Sets the color to red
        radio.send("0,0,0,0,0,0,0")         # Send 0 values. For not moving the mouse
        if(button_b.is_pressed()):          # If the B-button get's pressed enable controller again
            disabled = False
            sleep(200)                      # sleep a bit to debounce
        pass
    if not disabled:                        # If controller is enabled proceed
        # The following are all similar. Get the detected gesture, add to the send-Sting and set the color in the specified LED range
        if accelerometer.is_gesture("up"):
            sendMe = sendMe + ",0"
            lightUp(11,15, 0, 255, 0)

        elif accelerometer.is_gesture("down"):
            sendMe = sendMe + ",1"
            lightUp(23,3, 0, 255, 0)

        elif accelerometer.is_gesture("left"):
            sendMe = sendMe + ",2"
            lightUp(17, 21, 0, 0, 255)

        elif accelerometer.is_gesture("right"):
            sendMe = sendMe + ",3"
            lightUp(5,9, 0, 0, 255)

        elif accelerometer.is_gesture("face up"):
            sendMe = sendMe + ",4"
            lightUp(1,24, 180, 180, 180)

        elif accelerometer.is_gesture("face down"):
            sendMe = sendMe + ",5"
            lightUp(1,24, 255, 100, 0)

        elif accelerometer.is_gesture("freefall"):
            sendMe = sendMe + ",6"
            lightUp(1,24, 0, 255, 0)

        elif accelerometer.is_gesture("shake"):
            sendMe = sendMe + ",7"
            lightUp(1,24, 255, 255, 0)

        else:                           # If none of these gestures was detected send -1
            sendMe = sendMe + ",-1"
    

        if button_a.is_pressed():       # If the button is pressed add 1 to the send-String if not then add 0
            sendMe = sendMe + ",1"
        else:
            sendMe = sendMe + ",0"

        radio.send(sendMe)              # Send the "sendMe"-String to the reciever

        if button_b.is_pressed():       # If button b is pressed, then disable the controller function
            disabled = True
            sleep(500)

        message = radio.receive()       # For talkback not used right now

        if(message):
            pass