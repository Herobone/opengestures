# Open Gestures
<img src="https://gitlab.com/Herobone/opengestures/-/raw/master/OpenGesturesLogo.png" width=500 height=500>

A project developed during the *Jugend Hackt event in Munich 2020*.
Open Gestures is a movement detector controlled via micro:bit[](https://microbit.org/).

Each movement has designated LEDs flashing on the micro:bit.
For example 5 blue LEDs flash on the left side if the device is tilted in that direction.
Other color schemes include a ring of red, orange or green LEDs

## Hardware:
We used two BBC micro:bits and equipped one with an Kitronic ZIP Halo

Code for serial processing: https://stackoverflow.com/questions/58043143/how-to-set-up-serial-communication-with-microbit-using-pyserial

Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
